﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Description;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

class ByzHost
{
    public static int N;
    public static int L;
    public static int I;
    public static int V;
    public static int V0;
    public static int F;
    public static string[] Lines;
    public static List<List<string>> FakeMessages = new List<List<string>>();

    static void Main(string[] args)
    {
        WebServiceHost host = null;

        args = new string[] { "4", "3", "1", "0", "0", "1", "byz1.txt" };

        ProcessCmdParameters(args);


        try
        {
            var baseAddress = new Uri($"http://localhost:{PortFor(I)}/");
            host = new WebServiceHost(typeof(ByzService), baseAddress);
            ServiceEndpoint ep = host.AddServiceEndpoint(typeof(INodeService), new WebHttpBinding(), "");

            host.Open();

            // http://localhost:8090/message?from=1,to=0,msg=...
            var msg = ($"Arc=0: {baseAddress}Message?from=?,to=?,msg=?");
            Console.Error.WriteLine(msg);
            Console.WriteLine(msg);
            ByzService.Done.WaitOne();
            //Console.Error.WriteLine ("Press <Enter> to stop the service.");
            //Console.ReadLine ();
            //ArcService.Done.WaitOne ();

            host.Close();

        }
        catch (Exception ex)
        {
            var msg = ($"*** Exception {ex.Message}");
            Console.Error.WriteLine(msg);
            Console.WriteLine(msg);
            host = null;

        }
        finally
        {
            if (host != null) ((IDisposable)host).Dispose();
        }
    }

    static void ProcessCmdParameters(string[] args)
    {
        bool n = int.TryParse(args[0], out N);
        if (!n || N < 1 || N > 9)
            throw new Exception("Argument N Incorrect");

        bool l = int.TryParse(args[1], out L);
        if (!l)
            throw new Exception("Argument L Incorrect");

        bool i = int.TryParse(args[2], out I);
        if (!i || I < 1 || I > N)
            throw new Exception("Arguments I Incorrect");

        bool v = int.TryParse(args[3], out V);
        if (!v)
            throw new Exception("Arguments V Incorrect");

        bool v0 = int.TryParse(args[4], out V0);
        if (!v0)
            throw new Exception("Arguments V0 Incorrect");

        bool f = int.TryParse(args[5], out F);
        if (!f)
            throw new Exception("Arguments F Incorrect");

        ProcessTxtFileForFaultyProcess(args);

        ByzService.Root = new Node
        {
            Source = 0,
            Val = ByzHost.V
        };
    }

    static void ProcessTxtFileForFaultyProcess(string[] args)
    {
        if (F != 1)
            return;
        if (args.Length < 6)
            throw new Exception("No textfile given as a parameter.");
        string fileName = args[6];
        string[] lines = File.ReadAllLines($"{Directory.GetCurrentDirectory()}\\{fileName}");
        int n = 1;
        for (int i = 0; i < lines.Length; i++)
        {
            string thisLine = lines[i].Replace(" ", string.Empty);
            if (i == 0)
            {
                n = 1;
            }
            else
            {
                n *= (N - i);
            }

            int length = n * N;
            if (thisLine.Length > length)
            {
                thisLine = thisLine.Substring(0, length);
            }
            if (thisLine.Length < length)
            {
                char lastChar = thisLine.Last();
                thisLine += lastChar * (length - thisLine.Length);
            }

            List<string> theseMessages = new List<string>();
            FakeMessages.Add(theseMessages);
            for (int j = 0; j < thisLine.Length; j += n)
            {
                theseMessages.Add(thisLine.Substring(j, n));
            }
        }
    }


    public static int PortFor(int n = 0)
    {
        return 8080 + n;
    }
}

class ByzService : INodeService
{
    public static AutoResetEvent Done = new AutoResetEvent(false);

    public static Func<int> Tid = () => Thread.CurrentThread.ManagedThreadId;

    public static Func<double> Millis = () => DateTime.Now.TimeOfDay.TotalMilliseconds;

    public static int PortFor(int n = 0)
    {
        return n == 0 ? 8090 : 8080 + n;
    }


    #region TreeProperty
    public static Node Root;

    public static int CurrentLevel = 0;

    #endregion


    public Message[] Messages(Message[] imsgs)
    {
        List<Message> messages = new List<Message>();

        if (CurrentLevel > ByzHost.L)
        {
            int intendedLevel = 2 * ByzHost.L + 1 - CurrentLevel;
            ReduceTree(Root, 0, intendedLevel);
            string displayedString = GetLeaves(Root, true);
            displayedString = getDisplayedString(displayedString);
            Console.WriteLine($"{CurrentLevel,2} {ByzHost.I,2} {displayedString}");
            var result = new List<Message>();
            for (int i = 1; i <= ByzHost.N; i++)
            {
                result.Add(new Message(imsgs[i - 1].Time, ByzHost.I, i, intendedLevel == 0 ? "done" : "continue"));
            }
            if (intendedLevel == 0)
                Done.Set();
            CurrentLevel += 1;
            return result.ToArray();
        }

        if (CurrentLevel != 0)
        {
            for (int i = 0; i < imsgs.Length; i++)
            {
                string msgContent = imsgs[i].Msg;
                int msgFrom = imsgs[i].From;
                List<int> vals = ExtractValsFromMessage(msgContent).ToList();
                ConstructTree(Root, msgFrom, vals, 0);
            }
        }

		string displayedStr = GetLeaves(Root, false);
        displayedStr = getDisplayedString(displayedStr);

        Console.WriteLine($"{CurrentLevel,2} {ByzHost.I,2} {displayedStr}");
        if (CurrentLevel == ByzHost.L)
        {
            var result = new List<Message>();
            for (int i = 1; i <= ByzHost.N; i++)
            {
                result.Add(new Message(imsgs[i - 1].Time, ByzHost.I, i, "continue"));
            }
            CurrentLevel += 1;
            return result.ToArray();
        }
        var messageStr = Recurse(Root);



        for (int i = 1; i <= ByzHost.N; i++)
            messages.Add(new Message(imsgs[0].Time + 1, ByzHost.I, i,
                ByzHost.F == 0 ? messageStr : ByzHost.FakeMessages.ElementAt(CurrentLevel).ElementAt(i - 1)));
        CurrentLevel += 1;
        return messages.ToArray();
    }

    public string getDisplayedString(string displayedStr)
    {
        if (displayedStr.Length > 1 && CurrentLevel <= ByzHost.L)
        {
            List<string> tempList = new List<string>();
            int length = 1;

            for (int i = 1; i <= CurrentLevel - 1; i++)
            {
                length *= (ByzHost.N - i);
            }

            for (int i = 0; i < displayedStr.Length; i += length)
            {
                tempList.Add(displayedStr.Substring(i, length));
            }

            displayedStr = string.Join(" ", tempList);
        } else if (displayedStr.Length > 1 && CurrentLevel > ByzHost.L)
        {
            List<string> tempList = new List<string>();
            int length = 1;

            for (int i = 1; i <= 2 * ByzHost.L - CurrentLevel; i++)
            {
                length *= (ByzHost.N - i);
            }

            for (int i = 0; i < displayedStr.Length; i += length)
            {
                tempList.Add(displayedStr.Substring(i, length));
            }

            displayedStr = string.Join(" ", tempList);
        }
        return displayedStr;
    }

    public int[] ExtractValsFromMessage(string message)
    {
        string[] splitedMessage = message.Split();
        int[] vals = new int[splitedMessage.Length * splitedMessage[0].Length];
        for (int i = 0; i < splitedMessage.Length; i++)
        {
            string thisMessage = splitedMessage[i];
            int offset = i * thisMessage.Length;

            for (int j = 0; j < thisMessage.Length; j++)
            {
                int thisVal = int.Parse(thisMessage[j].ToString());
                vals[j + offset] = thisVal;
            }
        }
        return vals;
    }

    public void ReduceTree(Node node, int level, int intendedLevel)
    {
        if (level >= intendedLevel)
        {
            if (node.Children.Count == 0)
            {
                if (node.Val == 2)
                {
                    node.NewVal = ByzHost.V0;
                }
                else
                {
                    node.NewVal = node.Val;
                }
                return;
            }
            int count0 = 0;
            int count1 = 0;
            for (int keyIndex = 0; keyIndex < node.Children.Keys.Count; keyIndex++)
            {
                var key = node.Children.Keys.ElementAt(keyIndex);
                var childNode = node.Children[key];
                if (childNode.NewVal == 0)
                {
                    count0 += 1;
                }
                if (childNode.NewVal == 1)
                {
                    count1 += 1;
                }
            }
            node.Children.Clear();

            if (count0 > count1)
            {
                node.NewVal = 0;
            }
            else if (count1 > count0)
            {
                node.NewVal = 1;
            }
            else
            {
                node.NewVal = ByzHost.V0;
            }

            return;
        }
        if (level < intendedLevel)
        {
            foreach (int key in node.Children.Keys)
            {
                ReduceTree(node.Children[key], level + 1, intendedLevel);
            }
        }
    }

    public void ConstructTree(Node node, int from, List<int> vals, int depth)
    {
        if (node.Source == from)
            return;
        if (node.Children.Count < ByzHost.N - depth)
        {

            node.Children.Add(from, new Node
            {
                Source = from,
                Val = vals.First()
            });
            vals.RemoveAt(0);
            return;
        };
        foreach (int key in node.Children.Keys)
        {
            ConstructTree(node.Children[key], from, vals, depth + 1);
        }
    }

    public string Recurse(Node node)
    {
        if (node.Source == ByzHost.I)
            return string.Empty;
        if (node.Children.Count == 0)
        {
            return $"{node.Val}";
        }
        var msg = string.Empty;
        foreach (var key in node.Children.Keys)
        {
            msg += Recurse(node.Children[key]);
        }
        return msg;
    }

    public string GetLeaves(Node node, bool getNewVal)
    {
        if (node.Children.Count == 0)
            return getNewVal ? node.NewVal.ToString() : node.Val.ToString();
        string result = string.Empty;
        foreach (var key in node.Children.Keys)
        {
            result += GetLeaves(node.Children[key], getNewVal);
        }
        return result;
    }
}

public class Node
{
    public int Val;
    public int NewVal;
    public int Source;
    public SortedDictionary<int, Node> Children = new SortedDictionary<int, Node>();
    public Node Parent;
}